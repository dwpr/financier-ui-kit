const jestConfig = {
  collectCoverage: true,
  coverageDirectory: "jest_coverage",
  roots: [
    "<rootDir>/src",
  ],
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  moduleNameMapper: {
    "\\.css$": "identity-obj-proxy"
  },
  setupFilesAfterEnv: [
    "./enzyme.config.js",
  ],
};

module.exports = jestConfig;
