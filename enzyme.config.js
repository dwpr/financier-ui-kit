const enzyme = require("enzyme");
const ReactSixteenAdapter = require("enzyme-adapter-react-16");

enzyme.configure({
  adapter: new ReactSixteenAdapter(),
});
