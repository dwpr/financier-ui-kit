declare module "*.svg" {
  const iconPath: string;

  export default iconPath;
}
