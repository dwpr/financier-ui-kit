import { formatNumber } from "../format-number";

test("formatNumber: should fill spaces", () => {
  const actualResult = formatNumber("123456");
  const expectedResult = "123 456";

  expect(actualResult)
    .toBe(expectedResult);
});

test("formatNumber: should not fill spaces when number less thousand", () => {
  const actualResult = formatNumber("123");
  const expectedResult = "123";

  expect(actualResult)
    .toBe(expectedResult);
});

test("formatNumber: should not fill spaces after determiner", () => {
  const actualResult = formatNumber("123456,789");
  const expectedResult = "123 456,789";

  expect(actualResult)
    .toBe(expectedResult);
});

test("formatNumber: should return empty if pass empty", () => {
  const actualResult = formatNumber("");
  const expectedResult = "";

  expect(actualResult)
    .toBe(expectedResult);
});
