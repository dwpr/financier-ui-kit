import React from "react";
import { render } from "react-dom";

import { App } from "./components/app/app";

const appRoot = document.getElementById("root");

const onAppRender = (): void => {
  const appPreloadOverlay = document.getElementById("appPreloadOverlay");

  if (appPreloadOverlay !== null) {
    document.body.removeChild(appPreloadOverlay);
  }
};

render(<App />, appRoot, onAppRender);
