import React, {
  Component,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import { Button, EButtonIcon } from "../../controls/button/button";
import { ETextButtonIcon, TextButton } from "../../controls/text-button/text-button";

import headerStyles from "./header.css";

export class Header extends Component {
  public render(): TReactElement {
    return (
      <div className={ headerStyles["header"] }>
        <div className={ headerStyles["header__navigation"] }>
          <h1 className={ headerStyles["header__title"] }>Платежи</h1>
          <TextButton
            text="Закрыть"
            icon={ ETextButtonIcon.CROSS }
          />
        </div>
        <div className={ headerStyles["header__settings"] }>
          <Button
            text="Добавить"
            icon={ EButtonIcon.ADD }
          />
        </div>
      </div>
    );
  }
}
