declare const headerStyles: {
  "header": string;
  "header__navigation": string;
  "header__settings": string;
  "header__title": string;
}

export = headerStyles;
