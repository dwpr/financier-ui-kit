import React from "react";

import { TReactElement } from "../../models/t-react-element";
import { DropdownButton } from "../controls/dropdown-button/dropdown-button";

import appStyles from "./app.css";
import { Header } from "./header/header";
import { Navigation } from "./navigation/navigation";

export const App = (): TReactElement =>
  (
    <div className={ appStyles["app"] }>

      <main className={ appStyles["app__content"] }>
        <DropdownButton
          options={ [
            { id: 1, name: "Option1" },
            { id: 2, name: "Option2" },
            { id: 3, name: "Option3" },
          ] }
          buttonText="Добавить"
        />
      </main>
      <Header />
      <Navigation />
    </div>
  );
