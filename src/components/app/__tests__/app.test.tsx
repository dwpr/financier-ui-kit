// tslint:disable-next-line: no-implicit-dependencies
import { mount } from "enzyme";
import React from "react";

import { App } from "../app";

test("App: should render", () => {
  const appWrapper = mount(<App />);

  expect(appWrapper.find(".app"))
    .toHaveLength(1);
});
