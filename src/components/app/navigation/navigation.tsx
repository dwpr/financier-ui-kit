import React from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";
import { IconMainNavAccounts } from "../../icons/icon-main-nav-accounts";
import { IconMainNavAgents } from "../../icons/icon-main-nav-agents";
import { IconMainNavDds } from "../../icons/icon-main-nav-dds";
import { IconMainNavDirectories } from "../../icons/icon-main-nav-directories";
import { IconMainNavDocuments } from "../../icons/icon-main-nav-documents";
import { IconMainNavOpiu } from "../../icons/icon-main-nav-opiu";
import { IconMainNavPayments } from "../../icons/icon-main-nav-payments";

import navigationStyles from "./navigation.css";

export const Navigation = (): TReactElement => (
  <div className={ navigationStyles["navigation"] }>
    <div className={ navigationStyles["navigation__logo"] } />
    <div className={ navigationStyles["navigation__link-container"] }>
      <div
        className={ mergeClassNames([
          navigationStyles["navigation-link"],
          navigationStyles["navigation-link--active"],
        ]) }
      >
        <IconMainNavOpiu className={ navigationStyles["navigation-link__icon"] } />
        <p className={ navigationStyles["navigation-link__title"] }>БДР</p>
      </div>
      <div className={ navigationStyles["navigation-link"] }>
        <IconMainNavDds className={ navigationStyles["navigation-link__icon"] } />
        <p className={ navigationStyles["navigation-link__title"] }>ДДС</p>
      </div>
      <div className={ navigationStyles["navigation-link"] }>
        <IconMainNavAgents className={ navigationStyles["navigation-link__icon"] } />
        <p className={ navigationStyles["navigation-link__title"] }>Контрагенты</p>
      </div>
      <div
        className={ mergeClassNames([
          navigationStyles["navigation-link"],
          navigationStyles["navigation-link--light"],
        ]) }
      >
        <IconMainNavDocuments className={ navigationStyles["navigation-link__icon"] } />
        <p className={ navigationStyles["navigation-link__title"] }>Документы</p>
      </div>
      <div
        className={ mergeClassNames([
          navigationStyles["navigation-link"],
          navigationStyles["navigation-link--light"],
        ]) }
      >
        <IconMainNavPayments className={ navigationStyles["navigation-link__icon"] } />
        <p className={ navigationStyles["navigation-link__title"] }>Платежи</p>
      </div>
      <div
        className={ mergeClassNames([
          navigationStyles["navigation-link"],
          navigationStyles["navigation-link--light"],
        ]) }
      >
        <IconMainNavAccounts className={ navigationStyles["navigation-link__icon"] } />
        <p className={ navigationStyles["navigation-link__title"] }>Счета</p>
      </div>
      <div
        className={ mergeClassNames([
          navigationStyles["navigation-link"],
          navigationStyles["navigation-link--light"],
        ]) }
      >
        <IconMainNavDirectories className={ navigationStyles["navigation-link__icon"] } />
        <p className={ navigationStyles["navigation-link__title"] }>Справочники</p>
        <div className={ navigationStyles["navigation__sublink"] }>
          <ul className={ navigationStyles["navigation__sublink-list"] }>
            <li className={ navigationStyles["navigation__sublink-item"] }>Банковские счета</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
);
