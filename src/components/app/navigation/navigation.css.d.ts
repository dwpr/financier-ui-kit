declare const navigationStyles: {
  "navigation": string;
  "navigation__logo": string;
  "navigation__link-container": string;
  "navigation-link": string;
  "navigation-link--light": string;
  "navigation-link--active": string;
  "navigation-link__icon": string;
  "navigation-link__title": string;
  "navigation__sublink": string;
  "navigation__sublink-list": string;
  "navigation__sublink-item": string;
}

export = navigationStyles;
