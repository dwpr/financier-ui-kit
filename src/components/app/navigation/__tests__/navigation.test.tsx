// tslint:disable-next-line: no-implicit-dependencies
import { shallow } from "enzyme";
import React from "react";

import { Navigation } from "../navigation";

test("Navigation: should render", () => {
  const navigationWrapper = shallow(<Navigation />);

  expect(navigationWrapper.find(".navigation"))
    .toHaveLength(1);
});

test("Navigation: should match snapshot", () => {
  const navigationWrapper = shallow(<Navigation />);

  expect(navigationWrapper)
    .toMatchSnapshot();
});
