declare const styles: {
    "helper": string;
    "helper__icon": string;
    "helper__panel": string;
    "helper__text": string;
    "helper__close": string;
    "helper__buttons": string;
}

export = styles