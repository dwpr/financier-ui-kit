// tslint:disable-next-line: no-implicit-dependencies
import { mount, shallow } from "enzyme";
import React from "react";

import { ETextButtonAppearance, TextButton } from "../text-button";

test("TextButton: should render text from props", () => {
  const textButtonProps = {
    text: "text",
  };

  const textButtonWrapper = shallow(<TextButton { ...textButtonProps } />);

  expect(textButtonWrapper.text())
    .toBe("text");
});

test("TextButton: should be primary by default", () => {
  const textButtonProps = {
    text: "text",
  };

  const textButtonWrapper = mount(<TextButton { ...textButtonProps } />);

  expect(
    textButtonWrapper.find(".text_button")
      .hasClass("text_button--primary"),
  )
  .toBe(true);
});

test("TextButton: should be primary", () => {
  const textButtonProps = {
    appearance: ETextButtonAppearance.PRIMARY,
    text: "text",
  };

  const textButtonWrapper = mount(<TextButton { ...textButtonProps } />);

  expect(
    textButtonWrapper.find(".text_button")
      .hasClass("text_button--primary"),
  )
  .toBe(true);
});

test("TextButton: should be secondary", () => {
  const textButtonProps = {
    appearance: ETextButtonAppearance.SECONDARY,
    text: "text",
  };

  const textButtonWrapper = mount(<TextButton { ...textButtonProps } />);

  expect(
    textButtonWrapper.find(".text_button")
      .hasClass("text_button--secondary"),
  )
    .toBe(true);
});

test("TextButton: should be success", () => {
  const textButtonProps = {
    appearance: ETextButtonAppearance.SUCCESS,
    text: "text",
  };

  const textButtonWrapper = mount(<TextButton { ...textButtonProps } />);

  expect(
    textButtonWrapper.find(".text_button")
      .hasClass("text_button--success"),
  )
    .toBe(true);
});
