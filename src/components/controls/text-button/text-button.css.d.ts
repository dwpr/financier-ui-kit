declare const textButtonStyles: {
  "text_button": string;
  "text_button--primary": string;
  "text_button--secondary": string;
  "text_button--success": string;
  "text_button--icon": string;
  "text_button--icon-cross": string;
};

export = textButtonStyles;
