import React, {
  ButtonHTMLAttributes,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import textButtonStyles from "./text-button.css";

export enum ETextButtonAppearance {
  PRIMARY,
  SECONDARY,
  SUCCESS,
}

export enum ETextButtonIcon {
  CROSS = "icon-navigation-close-16",
}

const getAppearanceClassName = (appearance?: ETextButtonAppearance): string => {
  switch (appearance) {
    case ETextButtonAppearance.PRIMARY:
      return textButtonStyles["text_button--primary"];
    case ETextButtonAppearance.SECONDARY:
      return textButtonStyles["text_button--secondary"];
    case ETextButtonAppearance.SUCCESS:
      return textButtonStyles["text_button--success"];
    default:
      return textButtonStyles["text_button--primary"];
  }
};

const getIconClassName = (icon?: ETextButtonIcon): string => {
  let currentClassName: string;
  const baseClassName = textButtonStyles["text_button--icon"];

  switch (icon) {
    case ETextButtonIcon.CROSS:
      currentClassName = textButtonStyles["text_button--icon-cross"];
      break;
    default:
      currentClassName = "";
  }

  return mergeClassNames([baseClassName, currentClassName]);
};

interface ITextButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  appearance?: ETextButtonAppearance;
  text: string;
  icon?: ETextButtonIcon;
}

export const TextButton = (props: ITextButtonProps): TReactElement => (
  <button
    { ...props }
    className={ mergeClassNames([
      textButtonStyles["text_button"],
      getAppearanceClassName(props.appearance),
      getIconClassName(props.icon),
      props.className,
    ]) }
  >
    { props.text }
  </button>
);
