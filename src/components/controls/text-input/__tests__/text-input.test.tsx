import {
  mount,
  shallow,
// tslint:disable-next-line: no-implicit-dependencies
} from "enzyme";
import React, {
  ChangeEvent,
} from "react";

import { TextInput } from "../text-input";

test("TextInput: should render", () => {
  const textInputWrapper = shallow(<TextInput />);

  expect(textInputWrapper.find(".text-input"))
    .toHaveLength(1);
});

test("TextInput: should not render label by default", () => {
  const textInputWrapper = shallow(<TextInput />);

  expect(textInputWrapper.find(".text-input__label"))
    .toHaveLength(0);
});

test("TextInput: should render label", () => {
  const textInputProps = {
    label: "Label",
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  expect(
    textInputWrapper.find(".text-input__label")
      .text(),
  )
    .toBe("Label");
});

test("TextInput: should not render help message by default", () => {
  const textInputWrapper = shallow(<TextInput />);

  expect(textInputWrapper.find(".text-input__help-message"))
    .toHaveLength(0);
});

test("TextInput: should render help message", () => {
  const textInputProps = {
    helpMessage: "Help message",
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  expect(
    textInputWrapper.find(".text-input__help-message")
      .text(),
  )
    .toBe("Help message");
});

test("TextInput: should not render help message if hasError is true", () => {
  const textInputProps = {
    errorMessage: "Error message",
    hasError: true,
    helpMessage: "Help message",
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  expect(textInputWrapper.find(".text-input__help-message"))
    .toHaveLength(0);

});

test("TextInput: should override help message with error message", () => {
  const textInputProps = {
    errorMessage: "Error message",
    hasError: true,
    helpMessage: "Help message",
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  expect(
    textInputWrapper.find(".text-input__error-message")
      .text(),
  )
    .toBe("Error message");

});

test("TextInput: should not render error message be default", () => {
  const textInputWrapper = shallow(<TextInput />);

  expect(textInputWrapper.find(".text-input__error-message"))
    .toHaveLength(0);
});

test("TextInput: should render error message", () => {
  const textInputProps = {
    errorMessage: "Error message",
    hasError: true,
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  expect(
    textInputWrapper.find(".text-input__error-message")
      .text(),
  )
    .toBe("Error message");
});

test("TextInput: should not render error message if hasError is undefined", () => {
  const textInputProps = {
    errorMessage: "Error message",
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  expect(textInputWrapper.find(".text-input__error-message"))
    .toHaveLength(0);
});

test("TextInput: should render default value", () => {
  const textInputProps = {
    value: "Hello, world!",
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  expect(
    textInputWrapper.find(".text-input")
      .props()
      .value,
  )
      .toBe("Hello, world!");
});

test("TextInput: should give value", () => {
  let actualInputValue;

  const textInputProps = {
    onChange: (event: ChangeEvent<HTMLInputElement>): void => {
      actualInputValue = event.currentTarget.value;
    },
  };

  const textInputWrapper = shallow(<TextInput { ...textInputProps } />);

  textInputWrapper.find(".text-input")
    .simulate("change", {
      currentTarget: {
        value: "Hello, world!",
      },
    });

  expect(actualInputValue)
    .toBe("Hello, world!");
});

test("TextInput: should set inputRef", () => {
  const textInputWrapper = mount<TextInput>(<TextInput />);

  expect(
    textInputWrapper.instance()
      .inputRef,
  )
    .toBeTruthy();
});
