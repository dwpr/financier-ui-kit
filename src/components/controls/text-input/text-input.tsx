import React, {
  Component,
  InputHTMLAttributes,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import textInputStyles from "./text-input.css";

export interface ITextInputProps extends InputHTMLAttributes<HTMLInputElement> {
  errorMessage?: string;
  hasError?: boolean;
  helpMessage?: string;
  label?: string;
}

export class TextInput extends Component<ITextInputProps> {
  public inputRef: HTMLInputElement | undefined;

  public render(): TReactElement {
    const {
      label,
      hasError,
      errorMessage,
      helpMessage,
      ...restProps
    } = this.props;

    return (
      <div>
        {
          label !== undefined &&
          <p className={ textInputStyles["text-input__label"] }>{ label }</p>
        }
        <input
          { ...restProps }
          ref={ this.setInputRef }
          type="text"
          className={ mergeClassNames([
            textInputStyles["text-input"],
            hasError ? textInputStyles["text-input--error"] : "",
            this.props.className,
          ]) }
        />
        {
          hasError &&
          <p className={ textInputStyles["text-input__error-message"] }>{ errorMessage }</p>
        }
        {
          !hasError && helpMessage !== undefined &&
          <p className={ textInputStyles["text-input__help-message"] }>{ helpMessage }</p>
        }
      </div>
    );
  }

  private readonly setInputRef = (node: HTMLInputElement): void => {
    this.inputRef = node;
  }
}
