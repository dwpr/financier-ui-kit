declare const textInputStyles: {
  "text-input": string;
  "text-input--error": string;
  "text-input__label": string;
  "text-input__error-message": string;
  "text-input__help-message": string;
}

export = textInputStyles;
