declare const textAreaStyles: {
  "textarea-container": string;
  "textarea-container--disabled": string;
  "textarea": string;
  "textarea__label": string;
  "textarea--disabled": string;
  "textarea__error-message": string;
  "textarea__help-message": string;
}

export = textAreaStyles;