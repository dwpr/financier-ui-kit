// tslint:disable-next-line:no-implicit-dependencies
import { mount, shallow } from "enzyme";
import React, { ChangeEvent } from "react";

import { TextArea } from "../text-area";

test("Textarea should render", () => {
  const textareaWrapper = shallow(<TextArea />);

  expect(textareaWrapper.find(".textarea"))
    .toHaveLength(1);
});

test("Textarea should not render label by default", () => {
  const textareaWrapper = shallow(<TextArea />);

  expect(textareaWrapper.find(".textarea__label"))
    .toHaveLength(0);
});

test("Textarea should render label", () => {
  const textAreaProps = {
    label: "Label",
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(
    textareaWrapper.find(".textarea__label")
      .text(),
  )
    .toBe("Label");
});

test("Texarea should not render help message by default", () => {
  const textareaWrapper = shallow(<TextArea />);

  expect(textareaWrapper.find("textarea__help-message"))
    .toHaveLength(0);
});

test("Texarea should render help message", () => {
  const textAreaProps = {
    helpMessage: "Help message",
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(
    textareaWrapper.find(".textarea__help-message")
      .text(),
  )
    .toBe("Help message");
});

test("Texarea should not render help message if hasError is true", () => {
  const textAreaProps = {
    errorMessage: "Error message",
    hasError: true,
    helpMessage: "Help message",
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(textareaWrapper.find(".textarea__help-message"))
    .toHaveLength(0);
});

test("Texarea should override help message with error message", () => {
  const textAreaProps = {
    errorMessage: "Error message",
    hasError: true,
    helpMessage: "Help message",
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(
    textareaWrapper.find(".textarea__error-message")
      .text(),
  )
    .toBe("Error message");
});

test("Textarea should not render error message by default", () => {
  const textareaWrapper = shallow(<TextArea />);

  expect(textareaWrapper.find(".textarea__error-message"))
    .toHaveLength(0);
});

test("Textarea should render error message", () => {
  const textAreaProps = {
    errorMessage: "error message",
    hasError: true,
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(
    textareaWrapper.find(".textarea__error-message")
    .text(),
  )
    .toBe("error message");
});

test("Textarea should not render error message if hasError is undefined", () => {
  const textAreaProps = {
    errorMessage: "error message",
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(textareaWrapper.find(".textarea__error-message"))
    .toHaveLength(0);
});

test("Textarea should render default value", () => {
  const textAreaProps = {
    value: "default value",
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(textareaWrapper.find(".textarea")
    .props()
    .value,
  )
    .toBe("default value");
});

test("Textarea should change value", () => {
  let actualValue;

  const textAreaProps = {
    onChange: (event: ChangeEvent<HTMLTextAreaElement>): void => {
      actualValue = event.target.value;
    },
  };

  const textareaWrapper = mount(<TextArea { ...textAreaProps } />);

  textareaWrapper.find(".textarea")
    .simulate("change", {
      target: {
        value: "new value",
      },
    });

  expect(actualValue)
    .toBe("new value");
});

test ("Textarea should not change value if disabled is true", () => {
  const textAreaProps = {
    disabled: true,
  };

  const textareaWrapper = shallow(<TextArea { ...textAreaProps } />);

  expect(textareaWrapper.find(".textarea-container--disabled"))
    .toHaveLength(1);
});

test("Textarea should not call change if it is undefined", () => {
  const actualValue = "first value";

  const textAreaProps = {
    value: actualValue,
  };

  const textareaWrapper = mount(<TextArea { ...textAreaProps } />);

  textareaWrapper.find(".textarea")
    .simulate("change", {
      target: {
        value: "new value",
      },
    });

  expect(actualValue)
    .toBe("first value");
});
