import React, {
  ChangeEvent,
  createRef,
  RefObject,
  TextareaHTMLAttributes,
  useState,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import textAreaStyles from "./text-area.css";

interface ITextareaProps extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  errorMessage?: string;
  hasError?: boolean;
  helpMessage?: string;
  label?: string;
}

export const TextArea = (props: ITextareaProps): TReactElement => {
  const {
    label,
    hasError,
    helpMessage,
    errorMessage,
    ...restProps
  } = props;

  const textareaRef: RefObject<HTMLTextAreaElement> = createRef<HTMLTextAreaElement>();

  const [ value, setValue ] = useState(props.value);

  const onValueChange = (event: ChangeEvent<HTMLTextAreaElement>): void => {
    event.persist();

    const { currentTarget } = event;
    const { onChange } = props;

    setValue(currentTarget.value);

    /* istanbul ignore next line */
    if (textareaRef.current) {
      const textareaStyles = getComputedStyle(textareaRef.current);
      const borderTopWidth = parseInt(textareaStyles.getPropertyValue("border-top-width"), 10);
      const borderBottomWidth = parseInt(textareaStyles.getPropertyValue("border-top-width"), 10);

      textareaRef.current.style.height = `${currentTarget.scrollHeight + borderTopWidth + borderBottomWidth}px`;
    }

    if (onChange) {
      onChange(event);
    }
  };

  return (
    <div
      className={ mergeClassNames([
        textAreaStyles["textarea-container"],
        props.disabled ? textAreaStyles["textarea-container--disabled"] : "",
        props.className,
      ]) }
    >
      {
        label &&
        <p className={ textAreaStyles["textarea__label"] }>{ label }</p>
      }
      <textarea
        { ...restProps }
        ref={ textareaRef }
        className={ textAreaStyles["textarea"] }
        value={ value }
        onChange={ onValueChange }
      />
      {
        hasError &&
        <p className={ textAreaStyles["textarea__error-message"] }>{ errorMessage }</p>
      }
      {
        !hasError && helpMessage &&
        <p className={ textAreaStyles["textarea__help-message"] }>{ helpMessage }</p>
      }
    </div>
  );
};
