import React from "react";

import { TReactElement } from "../../../models/t-react-element";

import styles from "./tooltip.css";

interface ITooltipProps extends React.HTMLAttributes<HTMLDivElement> {
  text: string;
  optional_text?: string;
}

export const Tooltip = (props: ITooltipProps): TReactElement => (
  <div
    { ...props }
    className={ styles["tooltip"] }
  >
    <p className={ styles["tooltip__panel"] }>
      { props.text }
      {
        props.optional_text
          ? (
              <p className={ styles["tooltip__optional-text"] }>
                  { props.optional_text }
              </p>
            )
          : null
      }
    </p>
  </div>
);
