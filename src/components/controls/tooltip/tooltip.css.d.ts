declare const styles: {
    "tooltip": string;
    "tooltip__panel": string;
    "tooltip__optional-text": string;
}

export = styles