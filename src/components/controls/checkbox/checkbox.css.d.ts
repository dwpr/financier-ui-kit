declare const checkboxStyles: {
  "checkbox": string;
  "checkbox--disabled": string;
  "checkbox__control": string;
  "checkbox__control--checked": string;
  "checkbox__control--unchecked": string;
  "checkbox__control--indeterminate": string;
  "checkbox__label": string;
}

export = checkboxStyles;
