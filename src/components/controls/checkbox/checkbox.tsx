import React, {
  useState,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import checkboxStyles from "./checkbox.css";

export enum ECheckboxState {
  CHECKED,
  UNCHECKED,
  INDETERMINATE,
}

const getStateClassName = (state: ECheckboxState): string => {
  switch (state) {
    case ECheckboxState.CHECKED:
      return checkboxStyles["checkbox__control--checked"];
    case ECheckboxState.UNCHECKED:
        return checkboxStyles["checkbox__control--unchecked"];
    case ECheckboxState.INDETERMINATE:
        return checkboxStyles["checkbox__control--indeterminate"];
    default:
      throw new Error("Current checkbox state is not support");
  }
};

interface ICheckboxProps {
  state: ECheckboxState;
  text?: string;
  disabled?: boolean;
  className?: string;
  onChange?(state: ECheckboxState): void;
}

export const Checkbox = (props: ICheckboxProps): TReactElement => {
  const [state, setState] = useState(props.state);

  const onClick = (): void => {
    if (props.disabled) {
      return;
    }

    const {
      onChange,
    } = props;

    let newCeckboxState: ECheckboxState = ECheckboxState.UNCHECKED;

    // В state не может оказаться ничего кроме ECheckboxState
    // tslint:disable-next-line:switch-default
    switch (state) {
      case ECheckboxState.CHECKED:
      case ECheckboxState.INDETERMINATE:
        newCeckboxState = ECheckboxState.UNCHECKED;
        break;
      case ECheckboxState.UNCHECKED:
        newCeckboxState = ECheckboxState.CHECKED;
    }

    setState(newCeckboxState);

    if (onChange) {
      onChange(newCeckboxState);
    }
  };

  return (
    <label
      className={ mergeClassNames([
        checkboxStyles["checkbox"],
        props.disabled ? checkboxStyles["checkbox--disabled"] : "",
      ]) }
      onClick={ onClick }
    >
      <div
        className={ mergeClassNames([
          checkboxStyles["checkbox__control"],
          getStateClassName(state),
          props.className,
        ]) }
      />
      {
        props.text !== undefined &&
        <p className={ checkboxStyles["checkbox__label"] }>{ props.text }</p>
      }
    </label>
  );
};
