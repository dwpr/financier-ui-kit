// tslint:disable-next-line: no-implicit-dependencies
import { shallow } from "enzyme";
import React from "react";

import { Checkbox, ECheckboxState } from "../checkbox";

test("Checkbox: should render", () => {
  const checkboxProps = {
    state: ECheckboxState.CHECKED,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  expect(checkboxWrapper.find(".checkbox"))
    .toHaveLength(1);
});

test("Checkbox: should not render text by default", () => {
  const checkboxProps = {
    state: ECheckboxState.CHECKED,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  expect(checkboxWrapper.find(".checkbox__label"))
    .toHaveLength(0);
});

test("Checkbox: should render text from props", () => {
  const checkboxProps = {
    state: ECheckboxState.CHECKED,
    text: "text",
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  expect(
    checkboxWrapper.find(".checkbox__label")
      .text(),
  )
    .toBe("text");
});

test("Checkbox: should be checked", () => {
  const checkboxProps = {
    state: ECheckboxState.CHECKED,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  expect(checkboxWrapper.find(".checkbox__control--checked"))
    .toHaveLength(1);
});

test("Checkbox: should be unchecked", () => {
  const checkboxProps = {
    state: ECheckboxState.UNCHECKED,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  expect(checkboxWrapper.find(".checkbox__control--unchecked"))
    .toHaveLength(1);
});

test("Checkbox: should be indeterminate", () => {
  const checkboxProps = {
    state: ECheckboxState.INDETERMINATE,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  expect(checkboxWrapper.find(".checkbox__control--indeterminate"))
    .toHaveLength(1);
});

test("Checkbox: should switch to checked from unchecked", () => {
  let actualState: ECheckboxState = ECheckboxState.UNCHECKED;

  const checkboxProps = {
    onChange: (state: ECheckboxState): void => {
      actualState = state;
    },
    state: ECheckboxState.UNCHECKED,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  checkboxWrapper.find(".checkbox")
    .simulate("click", {});

  expect(actualState)
    .toBe(ECheckboxState.CHECKED);
});

test("Checkbox: should switch to unchecked from checked", () => {
  let actualState: ECheckboxState = ECheckboxState.CHECKED;

  const checkboxProps = {
    onChange: (state: ECheckboxState): void => {
      actualState = state;
    },
    state: ECheckboxState.CHECKED,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  checkboxWrapper.find(".checkbox")
    .simulate("click", {});

  expect(actualState)
    .toBe(ECheckboxState.UNCHECKED);
});

test("Checkbox: should switch to unchecked from indeterminate", () => {
  let actualState: ECheckboxState = ECheckboxState.INDETERMINATE;

  const checkboxProps = {
    onChange: (state: ECheckboxState): void => {
      actualState = state;
    },
    state: ECheckboxState.INDETERMINATE,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  checkboxWrapper.find(".checkbox")
    .simulate("click", {});

  expect(actualState)
    .toBe(ECheckboxState.UNCHECKED);
});

test("Checkbox: should throw error when state is not supported", () => {
  const checkboxProps = {
    state: 6,
  };

  expect(() => {
    shallow(<Checkbox { ...checkboxProps } />);
  })
    .toThrow("Current checkbox state is not support");
});

test("Checkbox: should switch to unchecked from indeterminate", () => {
  let actualState: ECheckboxState = ECheckboxState.INDETERMINATE;

  const checkboxProps = {
    onChange: (state: ECheckboxState): void => {
      actualState = state;
    },
    state: ECheckboxState.INDETERMINATE,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  checkboxWrapper.find(".checkbox")
    .simulate("click", {});

  expect(actualState)
    .toBe(ECheckboxState.UNCHECKED);
});

test("Checkbox: should not call change if it's undefined", () => {
  const actualState: ECheckboxState = ECheckboxState.INDETERMINATE;

  const checkboxProps = {
    state: ECheckboxState.INDETERMINATE,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  checkboxWrapper.find(".checkbox")
    .simulate("click", {});

  expect(actualState)
    .toBe(ECheckboxState.INDETERMINATE);
});

test("Checkbox: should not call change if it's undefined", () => {
  const actualState: ECheckboxState = ECheckboxState.UNCHECKED;

  const checkboxProps = {
    state: ECheckboxState.UNCHECKED,
  };

  const checkboxWrapper = shallow(<Checkbox { ...checkboxProps } />);

  checkboxWrapper.find(".checkbox")
    .simulate("click", {});

  expect(actualState)
    .toBe(ECheckboxState.UNCHECKED);
});
