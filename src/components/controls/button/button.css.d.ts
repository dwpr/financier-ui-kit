declare const buttonStyles: {
  "button": string;
  "button--small": string;
  "button--medium": string;
  "button--large": string;
  "button--primary": string;
  "button--secondary": string;
  "button--success": string;
  "button--light": string;
  "button--ghost": string;
  "button--icon": string;
  "button--icon-add": string;
};

export = buttonStyles;
