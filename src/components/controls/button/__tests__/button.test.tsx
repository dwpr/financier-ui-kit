// tslint:disable-next-line: no-implicit-dependencies
import { shallow } from "enzyme";
import React from "react";

import { Button, EButtonAppearance, EButtonSize } from "../button";

test("Button: should render text from props", () => {
  const buttonProps = {
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(buttonWrapper.text())
    .toBe("text");
});

test("Button: should be primary by default", () => {
  const buttonProps = {
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--primary"),
  )
  .toBe(true);
});

test("Button: should be large by default", () => {
  const buttonProps = {
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--large"),
  )
    .toBe(true);
});

test("Button: should be primary", () => {
  const buttonProps = {
    appearance: EButtonAppearance.PRIMARY,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--primary"),
  )
  .toBe(true);
});

test("Button: should be secondary", () => {
  const buttonProps = {
    appearance: EButtonAppearance.SECONDARY,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--secondary"),
  )
    .toBe(true);
});

test("Button: should be success", () => {
  const buttonProps = {
    appearance: EButtonAppearance.SUCCESS,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--success"),
  )
    .toBe(true);
});

test("Button: should be light", () => {
  const buttonProps = {
    appearance: EButtonAppearance.LIGHT,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--light"),
  )
    .toBe(true);
});

test("Button: should be ghost", () => {
  const buttonProps = {
    appearance: EButtonAppearance.GHOST,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--ghost"),
  )
    .toBe(true);
});

test("Button: should be small", () => {
  const buttonProps = {
    size: EButtonSize.SMALL,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--small"),
  )
    .toBe(true);
});

test("Button: should be medium", () => {
  const buttonProps = {
    size: EButtonSize.MEDIUM,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--medium"),
  )
    .toBe(true);
});

test("Button: should be large", () => {
  const buttonProps = {
    size: EButtonSize.LARGE,
    text: "text",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  expect(
    buttonWrapper.find(".button")
      .hasClass("button--large"),
  )
    .toBe(true);
});

test("Button: should call onChange", () => {
  let isCall = false;

  const buttonProps = {
    onClick: (): void => {
      isCall = true;
    },
    text: "Click Me!",
  };

  const buttonWrapper = shallow(<Button { ...buttonProps } />);

  buttonWrapper.find(".button")
    .simulate("click", {});

  expect(isCall)
    .toBe(true);
});
