import React, {
  ButtonHTMLAttributes,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import buttonStyles from "./button.css";

export enum EButtonAppearance {
  PRIMARY,
  SECONDARY,
  SUCCESS,
  LIGHT,
  GHOST,
}

export enum EButtonSize {
  SMALL,
  MEDIUM,
  LARGE,
}

export enum EButtonIcon {
  ADD = "icon-navigation-add-16",
}

const getAppearanceClassName = (appearance?: EButtonAppearance): string => {
  switch (appearance) {
    case EButtonAppearance.PRIMARY:
      return buttonStyles["button--primary"];
    case EButtonAppearance.SECONDARY:
      return buttonStyles["button--secondary"];
    case EButtonAppearance.SUCCESS:
      return buttonStyles["button--success"];
    case EButtonAppearance.LIGHT:
      return buttonStyles["button--light"];
    case EButtonAppearance.GHOST:
      return buttonStyles["button--ghost"];
    default:
      return buttonStyles["button--primary"];
  }
};

const getSizeClassName = (size?: EButtonSize): string => {
  switch (size) {
    case EButtonSize.SMALL:
      return buttonStyles["button--small"];
    case EButtonSize.MEDIUM:
      return buttonStyles["button--medium"];
    case EButtonSize.LARGE:
        return buttonStyles["button--large"];
    default:
      return buttonStyles["button--large"];
  }
};

const getIconClassName = (icon?: EButtonIcon): string => {
  let currentClassName: string;
  const baseClassName = buttonStyles["button--icon"];

  switch (icon) {
    case EButtonIcon.ADD:
      currentClassName = buttonStyles["button--icon-add"];
      break;
    default:
      currentClassName = "";
  }

  return mergeClassNames([baseClassName, currentClassName]);
};

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  appearance?: EButtonAppearance;
  size?: EButtonSize;
  text: string;
  icon?: EButtonIcon;
}

export const Button = (props: IButtonProps): TReactElement => (
  <button
    { ...props }
    className={ mergeClassNames([
      buttonStyles["button"],
      getAppearanceClassName(props.appearance),
      getSizeClassName(props.size),
      getIconClassName(props.icon),
      props.className,
    ]) }
  >
    { props.text }
  </button>
);
