declare const notificationStyles: {
  "notification": string;
  "notification--success": string;
  "notification--error": string;
  "notification--warning": string;
  "notification--transactional": string;
  "notification__icon": string;
  "notification__header": string;
  "notification__text": string;
  "notification__close": string;
  "notification__buttons": string;
}

export = notificationStyles