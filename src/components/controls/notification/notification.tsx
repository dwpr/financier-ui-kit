import React from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";
import { Button, EButtonAppearance } from "../button/button";

import notificationStyles from "./notification.css";

export enum ENotificationAppearance {
  SUCCESS,
  ERROR,
  WARNING,
  TRANSACTIONAL,
}

const getAppearanceClassName = (appearance: ENotificationAppearance): string => {
  switch (appearance) {
    case ENotificationAppearance.SUCCESS:
      return notificationStyles["notification--success"];
    case ENotificationAppearance.ERROR:
      return notificationStyles["notification--error"];
    case ENotificationAppearance.WARNING:
      return notificationStyles["notification--warning"];
    case ENotificationAppearance.TRANSACTIONAL:
      return notificationStyles["notification--transactional"];
    default:
      return notificationStyles["notification--error"];
  }
};

interface INotificationProps extends React.HTMLAttributes<HTMLDivElement> {
  appearance: ENotificationAppearance;
  header?: string;
  text: string;
}

export const Notification = (props: INotificationProps): TReactElement => (
  <div
    { ...props }
    className={ mergeClassNames([
      notificationStyles["notification"],
      getAppearanceClassName(props.appearance),
    ]) }
  >
    {
      props.header
        ? (
            <h6 className={ notificationStyles["notification__header"] }>
                { props.header }
            </h6>
          )
        : null
    }
    <p className={ notificationStyles["notification__text"] }>
      { props.text }
    </p>
    <div className={ notificationStyles["notification__close"] } />
    {
      props.appearance === ENotificationAppearance.TRANSACTIONAL
      ? (
          <div className={ notificationStyles["notification__buttons"] }>
              <Button
                appearance={ EButtonAppearance.GHOST }
                text="Decline"
              />
              <Button
                appearance={ EButtonAppearance.PRIMARY }
                text="Accept"
              />
          </div>
        )
      : null
    }
  </div>
);
