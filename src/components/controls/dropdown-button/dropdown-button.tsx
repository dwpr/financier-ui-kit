import React from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";
import { Button, EButtonAppearance, EButtonIcon, EButtonSize } from "../button/button";
import { DropdownList, IOpiton } from "../dropdown-list/dropdown-list";

import dropdownButtonStyles from "./dropdown-button.css";

interface IDropdownButton {
  options: IOpiton[];
  buttonText: string;
  buttonClassName?: string;
}

export const DropdownButton = (props: IDropdownButton): TReactElement => {
  const { buttonText, buttonClassName, options } = props;

  return (
    <div className={ dropdownButtonStyles["dropdown-button"] }>
      <div className={ dropdownButtonStyles["dropdown-button__button-container"] }>
        <Button
          appearance={ EButtonAppearance.PRIMARY }
          text={ buttonText }
          size={ EButtonSize.LARGE }
          className={ mergeClassNames([
            buttonClassName,
            dropdownButtonStyles["dropdown-button__button"]])
          }
          icon={ EButtonIcon.ADD }
        />
      </div>
      <DropdownList
        options={ options }
        className={ dropdownButtonStyles["dropdown-button__dropdown-list"] }
      />
    </div>
  );
};
