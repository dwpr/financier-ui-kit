declare const dropdownButtonStyles: {
    "dropdown-button": string;
    "dropdown-button__button": string;
    "dropdown-button__button-container": string;
    "dropdown-button__dropdown-list": string;
}

export = dropdownButtonStyles;