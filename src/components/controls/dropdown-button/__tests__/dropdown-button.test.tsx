// tslint:disable-next-line: no-implicit-dependencies
import { shallow } from "enzyme";
import React from "react";

import { DropdownButton } from "../dropdown-button";

test("DropdownButton: should render", () => {
  const dropdownButtonProps = {
    buttonText: "Primary button",
    options: [
      { id: 1, name: "Option 1" },
    ],
  };

  const dropdownButtonWrapper = shallow(<DropdownButton { ...dropdownButtonProps } />);
  expect(dropdownButtonWrapper.find(".dropdown-button"))
    .toHaveLength(1);
});
