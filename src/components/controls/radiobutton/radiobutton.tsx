import React from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import * as styles from "./radiobutton.css";

export enum RadiobuttonAppearance {
    BLOCK,
    BLOCK_WITH_COMMENT,
}

// It cold be automatized, but we are in love with props and in fear of designer
export enum RadiobuttonWidth {
    NARROW, // If caption.length >= 30
    WIDE, // If caption.length < 30
}

interface IRadiobuttonProps extends React.InputHTMLAttributes<HTMLInputElement> {
    name: string;
    value: string;
    appearance?: RadiobuttonAppearance;
    width?: RadiobuttonWidth;
    caption: string;
    comment?: string;
    checked?: boolean;
    disabled?: boolean;
}

const getRadiobuttonAppearanceClassName = (appearance?: RadiobuttonAppearance): string => {
    switch (appearance) {
        case RadiobuttonAppearance.BLOCK:
            return styles["radiobutton--block"];
        case RadiobuttonAppearance.BLOCK_WITH_COMMENT:
            return styles["radiobutton--block-comment"];
        default:
            return "";
    }
};

const getRadiobuttonWidthClassName = (width?: RadiobuttonWidth): string => {
    switch (width) {
        case RadiobuttonWidth.NARROW:
            return styles["radiobutton--narrow"];
        case RadiobuttonWidth.WIDE:
            return styles["radiobutton--wide"];
        default:
            return "";
    }
};

export const Radiobutton = (props: IRadiobuttonProps): TReactElement => (
    <label
        className={ mergeClassNames([
            styles["radiobutton"],
            props.className,
            getRadiobuttonAppearanceClassName(props.appearance),
            getRadiobuttonWidthClassName(props.width),
        ]) }
    >
        <input
            type="radio"
            { ...props }
        />
        <div className={ styles["radiobutton__wrapper"] }>
            <div className={ styles["radiobutton__container"] }>
                <div className={ styles["radiobutton__visualization"] } />
            </div>
            <span className={ styles["radiobutton__caption"] }>{ props.caption }</span>
            {
                RadiobuttonAppearance.BLOCK_WITH_COMMENT
                  ? (
                      <p className={ styles["radiobutton__comment"] }>
                        { props.comment }
                      </p>
                    )
                  : null
            }
        </div>
    </label>
);
