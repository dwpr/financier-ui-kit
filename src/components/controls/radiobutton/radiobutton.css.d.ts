declare const styles: {
    'radiobutton': string;
    'radiobutton__wrapper': string;
    'radiobutton__container': string;
    'radiobutton__visualization': string;
    'radiobutton__caption': string;
    'radiobutton__comment': string;
    'radiobutton--block': string;
    'radiobutton--block-comment': string;
    'radiobutton--narrow': string;
    'radiobutton--wide': string;
}

export = styles;