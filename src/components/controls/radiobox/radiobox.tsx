import React, {
  createContext,
  InputHTMLAttributes,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import radioboxStyles from "./radiobox.css";

export enum ERadioboxDirection {
  HORIZONTAL,
  VERTICAL,
}

interface IRadioboxProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  direction?: ERadioboxDirection;
}

export type TRadioboxContext = InputHTMLAttributes<HTMLInputElement>;

export const RadioboxContext = createContext<TRadioboxContext>({});

const getDirectionClassName = (direction?: ERadioboxDirection): string => {
  switch (direction) {
    case ERadioboxDirection.HORIZONTAL:
      return radioboxStyles["radiobox__wrapper--horizontal"];
    case ERadioboxDirection.VERTICAL:
      return radioboxStyles["radiobox__wrapper--vertical"];
    default:
      return radioboxStyles["radiobox__wrapper--horizontal"];
  }
};

export const Radiobox = (props: IRadioboxProps): TReactElement => {
  const {
    children,
    label,
    direction,
    ...restProps
  } = props;

  return (
    <RadioboxContext.Provider value={ restProps }>
      {
        label !== undefined &&
        <p className={ radioboxStyles["radiobox__label"] }>{ label }</p>
      }
      <div
        className={ mergeClassNames([
          radioboxStyles["radiobox__wrapper"],
          getDirectionClassName(direction),
          props.className,
        ]) }
      >
        { children }
      </div>
    </RadioboxContext.Provider>
  );
};
