// tslint:disable-next-line: no-implicit-dependencies
import { shallow } from "enzyme";
import React from "react";

import { RadioButton } from "../radio-button/radio-button";
import {
  ERadioboxDirection,
  Radiobox,
} from "../radiobox";

test("Radiobox: should render", () => {
  const radioboxWrapper = shallow(<Radiobox />);

  expect(radioboxWrapper.find(".radiobox__wrapper"))
    .toHaveLength(1);
});

test("Radiobox: should not render label by default", () => {
  const radioboxWrapper = shallow(<Radiobox />);

  expect(radioboxWrapper.find(".radiobox__label"))
    .toHaveLength(0);
});

test("Radiobox: should be horizontal by default", () => {
  const radioboxWrapper = shallow(<Radiobox />);

  expect(radioboxWrapper.find(".radiobox__wrapper--horizontal"))
    .toHaveLength(1);
});

test("Radiobox: should be horizontal", () => {
  const radioboxProps = {
    direction: ERadioboxDirection.HORIZONTAL,
  };

  const radioboxWrapper = shallow(<Radiobox { ...radioboxProps } />);

  expect(radioboxWrapper.find(".radiobox__wrapper--horizontal"))
    .toHaveLength(1);
});

test("Radiobox: should be vertical", () => {
  const radioboxProps = {
    direction: ERadioboxDirection.VERTICAL,
  };

  const radioboxWrapper = shallow(<Radiobox { ...radioboxProps } />);

  expect(radioboxWrapper.find(".radiobox__wrapper--vertical"))
    .toHaveLength(1);
});

test("Radiobox: should render label", () => {
  const radioboxProps = {
    label: "Label",
  };

  const radioboxWrapper = shallow(<Radiobox { ...radioboxProps } />);

  expect(
    radioboxWrapper.find(".radiobox__label")
      .text(),
  )
    .toBe("Label");
});

test("Radiobox: should not render radio buttons by default", () => {
  const radioboxWrapper = shallow((<Radiobox />));

  expect(radioboxWrapper.find(RadioButton))
    .toHaveLength(0);
});

test("Radiobox: should render radio buttons", () => {
  const radioboxWrapper = shallow((
    <Radiobox>
      <RadioButton />
    </Radiobox>
  ));

  expect(radioboxWrapper.find(RadioButton))
    .toHaveLength(1);
});
