declare const radioboxStyles: {
  "radiobox__label": string;
  "radiobox__wrapper": string;
  "radiobox__wrapper--horizontal": string;
  "radiobox__wrapper--vertical": string;
}

export = radioboxStyles;
