// tslint:disable-next-line: no-implicit-dependencies
import { shallow } from "enzyme";
import React from "react";

import { RadioButton } from "../radio-button";

test("RadioButton: should render", () => {
  const radioButtonWrapper = shallow(<RadioButton />);

  expect(radioButtonWrapper.find(".radio-button"))
    .toHaveLength(1);
});

test("RadioButton: should render custom control", () => {
  const radioButtonWrapper = shallow(<RadioButton />);

  expect(radioButtonWrapper.find(".radio-button__custom-control"))
    .toHaveLength(1);
});

test("RadioButton: should not render text by default", () => {
  const radioButtonWrapper = shallow(<RadioButton />);

  expect(radioButtonWrapper.find(".radio-button__label"))
    .toHaveLength(0);
});

test("RadioButton: should render text", () => {
  const radioButtonProps = {
    text: "Text",
  };

  const radioButtonWrapper = shallow(<RadioButton { ...radioButtonProps } />);

  expect(
    radioButtonWrapper.find(".radio-button__label")
      .text(),
  )
    .toBe("Text");
});
