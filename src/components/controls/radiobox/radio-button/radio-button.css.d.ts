declare const radioButtonStyles: {
  "radio-button": string;
  "radio-button__custom-control": string;
  "radio-button__native-control": string;
  "radio-button__label": string;
}

export = radioButtonStyles;
