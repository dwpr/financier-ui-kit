import React, {
  Component,
  Context,
  InputHTMLAttributes,
} from "react";

import { TReactElement } from "../../../../models/t-react-element";
import { mergeClassNames } from "../../../../utils/merge-class-names/merge-class-names";
import { RadioboxContext, TRadioboxContext } from "../radiobox";

import radioButtonStyles from "./radio-button.css";

interface IRadioButtonProps extends InputHTMLAttributes<HTMLInputElement> {
  text?: string;
}

export class RadioButton extends Component<IRadioButtonProps> {
  public context: TRadioboxContext | undefined;
  public static contextType: Context<TRadioboxContext> = RadioboxContext;

  public render(): TReactElement {
    const {
      text,
      ...restProps
    } = this.props;

    return (
      <label className={ radioButtonStyles["radio-button"] }>
        <input
          { ...this.context }
          { ...restProps }
          checked={ this.context && this.context.value === this.props.value }
          className={ mergeClassNames([
            radioButtonStyles["radio-button__native-control"],
            this.props.className,
           ]) }
          type="radio"
        />
        <div className={ radioButtonStyles["radio-button__custom-control"] } />
        {
          text !== undefined &&
          <p className={ radioButtonStyles["radio-button__label"] }>{ text }</p>
        }
      </label>
    );
  }
}
