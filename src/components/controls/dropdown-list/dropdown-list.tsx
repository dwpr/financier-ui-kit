import React from "react";

import { TReactElement } from "../../../models/t-react-element";
import { mergeClassNames } from "../../../utils/merge-class-names/merge-class-names";

import dropdownListStyles from "./dropdown-list.css";

export interface IOpiton {
  id: number;
  name: string;
}

interface IDropdownList {
  options: IOpiton[];
  className?: string;
}

export const DropdownList = (props: IDropdownList): TReactElement => {
  const { options } = props;

  return (
    <ul
      className={ mergeClassNames([
        dropdownListStyles["dropdown-list"],
        props.className,
      ]) }
    >
      {
        options.map((option: IOpiton) => (
          <li
            key={ option.id }
            className={ dropdownListStyles["dropdown-list__item"] }
          >
            { option.name }
          </li>
        ))
      }
    </ul>
  );
};
