// tslint:disable-next-line: no-implicit-dependencies
import { mount } from "enzyme";
import React, {
  ChangeEvent,
} from "react";

import { NumberInput } from "../number-input";

test("NumberInput: should render", () => {
  const numberInputWrapper = mount(<NumberInput />);

  expect(numberInputWrapper.find(".text-input"))
    .toHaveLength(1);
});

test("NumberInput: should not render label by default", () => {
  const numberInputWrapper = mount(<NumberInput />);

  expect(numberInputWrapper.find(".text-input__label"))
    .toHaveLength(0);
});

test("NumberInput: should render label", () => {
  const numberInputProps = {
    label: "Label",
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  expect(
    numberInputWrapper.find(".text-input__label")
      .text(),
  )
    .toBe("Label");
});

test("NumberInput: should not render help message by default", () => {
  const numberInputWrapper = mount(<NumberInput />);

  expect(numberInputWrapper.find(".text-input__help-message"))
    .toHaveLength(0);
});

test("NumberInput: should render help message", () => {
  const numberInputProps = {
    helpMessage: "Help message",
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  expect(
    numberInputWrapper.find(".text-input__help-message")
      .text(),
  )
    .toBe("Help message");
});

test("NumberInput: should not render help message if hasError is true", () => {
  const numberInputProps = {
    errorMessage: "Error message",
    hasError: true,
    helpMessage: "Help message",
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  expect(numberInputWrapper.find(".text-input__help-message"))
    .toHaveLength(0);

});

test("NumberInput: should override help message with error message", () => {
  const numberInputProps = {
    errorMessage: "Error message",
    hasError: true,
    helpMessage: "Help message",
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  expect(
    numberInputWrapper.find(".text-input__error-message")
      .text(),
  )
    .toBe("Error message");

});

test("NumberInput: should not render error message be default", () => {
  const numberInputWrapper = mount(<NumberInput />);

  expect(numberInputWrapper.find(".text-input__error-message"))
    .toHaveLength(0);
});

test("NumberInput: should render error message", () => {
  const numberInputProps = {
    errorMessage: "Error message",
    hasError: true,
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  expect(
    numberInputWrapper.find(".text-input__error-message")
      .text(),
  )
    .toBe("Error message");
});

test("NumberInput: should not render error message if hasError is undefined", () => {
  const numberInputProps = {
    errorMessage: "Error message",
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  expect(numberInputWrapper.find(".text-input__error-message"))
    .toHaveLength(0);
});

test("NumberInput: should set devault value", () => {
  const numberInputProps = {
    value: "123",
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  expect(numberInputWrapper.state<string>("value"))
    .toBe("123");
});

test("NumberInput: should give value", () => {
  let actualInputValue;

  const numberInputProps = {
    onChange: (event: ChangeEvent<HTMLInputElement>): void => {
      actualInputValue = event.target.value;
    },
  };

  const numberInputWrapper = mount(<NumberInput { ...numberInputProps } />);

  numberInputWrapper.find(".text-input")
    .simulate("change", {
      target: {
        value: "123",
      },
    });

  expect(actualInputValue)
    .toBe("123");
});

test("NumberInput: should change value", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper.find(".text-input")
    .simulate("change", {
      target: {
        value: "123",
      },
    });

  expect(numberInputWrapper.state<string>("value"))
    .toBe("123");
});

test("NumberInput: should not pass not number characters", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper.find(".text-input")
    .simulate("change", {
      target: {
        value: "ab12ab3ab",
      },
    });

  expect(numberInputWrapper.state<string>("value"))
    .toBe("123");
});

test("NumberInput: should set leading zero", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper.find(".text-input")
    .simulate("change", {
      target: {
        value: ",",
      },
    });

  expect(numberInputWrapper.state<string>("value"))
    .toBe("0,");
});

test("NumberInput: should set determiner after zero", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper.find(".text-input")
    .simulate("change", {
      target: {
        value: "0",
      },
    });

  expect(numberInputWrapper.state<string>("value"))
    .toBe("0,");
});

test("NumberInput: should delete excess determiner", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper.find(".text-input")
    .simulate("change", {
      target: {
        value: "0,123,,",
      },
    });

  expect(numberInputWrapper.state<string>("value"))
    .toBe("0,123");
});

test("NumberInput: should delete excess determiner on blur", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper.setState({
    value: "123,",
  });

  numberInputWrapper.find(".text-input")
    .simulate("blur", {});

  expect(numberInputWrapper.state<string>("value"))
    .toBe("123");
});

test("NumberInput: should delete insignificant zeros", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper
    .setState({
      value: "123000",
    });

  numberInputWrapper.find(".text-input")
    .simulate("blur", {});

  expect(numberInputWrapper.state<string>("value"))
    .toBe("123");
});

test("NumberInput: should delete leading zeros", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper
    .setState({
      value: "00000123",
    });

  numberInputWrapper.find(".text-input")
    .simulate("blur", {});

  expect(numberInputWrapper.state<string>("value"))
    .toBe("123");
});

test("NumberInput: should delete leading determiner", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper
    .setState({
      value: ",123",
    });

  numberInputWrapper.find(".text-input")
    .simulate("blur", {});

  expect(numberInputWrapper.state<string>("value"))
    .toBe("123");
});

test("NumberInput: should not delete signle zero", () => {
  const numberInputWrapper = mount(<NumberInput />);

  numberInputWrapper
    .setState({
      value: "0",
    });

  numberInputWrapper.find(".text-input")
    .simulate("blur", {});

  expect(numberInputWrapper.state<string>("value"))
    .toBe("0");
});

test("NumberInput: should set inputRef", () => {
  const numberInputWrapper = mount<NumberInput>(<NumberInput />);

  expect(
    numberInputWrapper.instance()
      .textInputRef,
  )
    .toBeTruthy();
});
