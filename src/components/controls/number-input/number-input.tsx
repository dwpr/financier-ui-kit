import React, {
  ChangeEvent,
  Component,
} from "react";

import { TReactElement } from "../../../models/t-react-element";
import {
  ITextInputProps,
  TextInput,
} from "../text-input/text-input";

interface INumberInputState {
  value: string;
}

interface IPreparedValue {
  preparedValue: string;
  selectionOffset: number;
}

export class NumberInput extends Component<ITextInputProps, INumberInputState> {
  public state: INumberInputState = {
    value: this.props.value !== undefined ? this.props.value.toString() : "",
  };

  public textInputRef: TextInput | undefined;

  public render(): TReactElement {
    const {
      value,
    } = this.state;

    return (
      <TextInput
        { ...this.props }
        ref={ this.setTextInputRef }
        value={ value }
        onChange={ this.onValueChange }
        onBlur={ this.onBlur }
      />
    );
  }

  private readonly onValueChange = (event: ChangeEvent<HTMLInputElement>): void => {
    event.persist();

    const {
      target: {
        value,
      },
    } = event;

    const {
      onChange,
    } = this.props;

    const {
      preparedValue,
      selectionOffset,
    } = this.prepareValue(value);

    const selectionStart = this.textInputRef && this.textInputRef.inputRef && this.textInputRef.inputRef.selectionStart;

    this.setState({
      value: preparedValue,
    }, () => {
      if (onChange) {
        onChange(event);

        /* istanbul ignore next line */
        if (this.textInputRef && this.textInputRef.inputRef) {
          if (selectionStart) {
            this.textInputRef.inputRef
              .setSelectionRange(selectionStart - selectionOffset, selectionStart - selectionOffset);
          }
        }
      }
    });
  }

  private readonly onBlur = (): void => {
    this.setState((prevState: INumberInputState) => {
      let preparedValue = prevState.value;

      if (preparedValue !== "0") {
        preparedValue = preparedValue.replace(/\,0+$/, "");
      }

      preparedValue = preparedValue.endsWith(",")
        ? preparedValue.slice(0, preparedValue.length - 1)
        : preparedValue;

      if (preparedValue !== "0") {
        if (preparedValue.startsWith("0") && !preparedValue.startsWith("0,")) {
          preparedValue = preparedValue.replace(/^0+/, "");
        }

        if (preparedValue.startsWith(",")) {
          preparedValue = preparedValue.slice(1, preparedValue.length);
        }

        preparedValue = preparedValue.replace(/0+$/, "");
      }

      return {
        value: preparedValue,
      };
    });
  }

  private readonly prepareValue = (value: string): IPreparedValue => {
    const {
      value: prevValue,
    } = this.state;

    let selectionOffset = 0;

    const isDeleting = prevValue.length > value.length;

    let preparedValue = value.replace(/\./g, ",");

    const preparedNumberValue = preparedValue.replace(/[^0-9,]/sgim, "");

    if (preparedNumberValue !== preparedValue) {
      selectionOffset = 1;
    }

    preparedValue = preparedNumberValue;

    /* istanbul ignore else  */
    if (!isDeleting) {
      if (preparedValue.startsWith("0") && !preparedValue.startsWith("0,")) {
        preparedValue = `0,${preparedValue.slice(1, preparedValue.length)}`;
        selectionOffset = -1;
      }

      if (preparedValue.startsWith(",")) {
        preparedValue = `0${preparedValue}`;
        selectionOffset = -1;
      }
    }

    let isSeparatorExist = false;

    preparedValue = preparedValue.split("")
      .map((char: string): string => {
        if (char === ",") {
          if (isSeparatorExist) {
            return "";
          }

          isSeparatorExist = true;
        }

        return char;
      })
      .join("");

    return {
      preparedValue,
      selectionOffset,
    };
  }

  private readonly setTextInputRef = (node: TextInput): void => {
    this.textInputRef = node;
  }
}
