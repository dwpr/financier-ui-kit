// tslint:disable:max-line-length
import React from "react";

import { TReactElement } from "../../models/t-react-element";

interface IIconProps {
  className?: string;
}

export const IconMainNavDirectories = (props: IIconProps): TReactElement => (
  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g id="icon-main_nav-directorys" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <path className={ props.className } d="M21.5,2 C21.7761424,2 22,2.22385763 22,2.5 L22,21.5 C22,21.7761424 21.7761424,22 21.5,22 L6.5,22 C4.01700015,21.9945005 2.00549947,19.9829999 2,17.5 L2,6.5 C2.00549947,4.01700015 4.01700015,2.00549947 6.5,2 L21.5,2 Z M3,17.5 C3.00134326,18.3486433 3.314529,19.1671969 3.88,19.8 C4.32685587,17.7261884 6.15859539,16.2441446 8.28,16.24 L21,16.24 L21,3 L6.5,3 C4.56700338,3 3,4.56700338 3,6.5 L3,17.5 Z M4.8,20.5 L4.8,20.54 C5.31661263,20.8395435 5.90283039,20.9981671 6.5,21 L21,21 L21,17.2 L8.28,17.2 C6.43021144,17.2075486 4.90568768,18.6532177 4.8,20.5 Z M10.5,9 C10.2238576,9 10,8.77614237 10,8.5 L10,5.5 C10,5.22385763 10.2238576,5 10.5,5 L18.5,5 C18.7761424,5 19,5.22385763 19,5.5 L19,8.5 C19,8.77614237 18.7761424,9 18.5,9 L10.5,9 Z M11,6 L11,8 L18,8 L18,6 L11,6 Z" id="icon-color" fill="#252B39"/>
    </g>
  </svg>
);
