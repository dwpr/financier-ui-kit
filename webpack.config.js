const { resolve} = require("path");
const { HotModuleReplacementPlugin } = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');

const webpackConfigBuilder = (env) => {
  const isProduction = env === "prod";

  return {
    mode: isProduction ? "production" : "development",
    devtool: isProduction ? "source-map" : "cheap-module-eval-source-map",
    entry: resolve(__dirname, "src", "index.tsx"),
    output: {
      filename: isProduction ? "script.[contenthash].js" : "script.[hash].js",
      path: resolve(__dirname, "dist"),
      publicPath: "/",
    },
    resolve: {
      extensions: [
        ".ts",
        ".tsx",
        ".css",
        ".svg",
        ".js",
      ],
    },
    module: {
      rules: [
        {
          test: /\.svg$/,
          use: [
            {
              loader: "file-loader",
              options: {
                outputPath: "public",
              },
            },
            {
              loader: "image-webpack-loader",
              options: {
                disable: isProduction ? false : true,
              },
            },
          ],
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                sourceMap: true,
                hmr: isProduction ? false : true,
              },
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                modules: true,
                localIdentName: "[local]____[hash:base64:10]____[name]",
                importLoaders: 1,
              },
            },
          ],
        },
        {
          test: /\.tsx?$/,
          use: [
            {
              loader: "ts-loader",
            },
          ],
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: isProduction ? "[name].[contenthash].css" : "[name].css",
      }),
      new HtmlWebpackPlugin({
        template: resolve(__dirname, "src", "index.html"),
      }),
      new CleanWebpackPlugin(),
      ...(isProduction ? [] : [new HotModuleReplacementPlugin()])
    ],
    optimization: {
      splitChunks: {
        chunks: "all",
      },
    },
    devServer: {
      port: 8288,
      historyApiFallback: true,
      contentBase: resolve(__dirname, "dist"),
      compress: true,
      hot: isProduction ? false : true,
      overlay: true,
    },
  };
};

module.exports = webpackConfigBuilder;
